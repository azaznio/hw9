%{
#include<stdio.h>
#include<string.h>
int FilterEnabled=0;
char Output[4096]="\0";
int CurrentColumnNumber=1;
int CurrentLineNumber=1;
%}
	
%%
 
 
[\n] {
    CurrentColumnNumber=1; 
    CurrentLineNumber=CurrentLineNumber+1; 
    char outputBuffer[128];
    sprintf(outputBuffer,
	"newLine(%d, %d, %d); ", 
	CurrentLineNumber, 
	CurrentColumnNumber, 
	CurrentColumnNumber-1+yyleng);
    strcat(Output, outputBuffer);
  }
[ ] {CurrentColumnNumber=CurrentColumnNumber+1;}
[\t] {
	CurrentColumnNumber=CurrentColumnNumber+1;
	char outputBuffer[128];
    sprintf(outputBuffer,
	"newColumn(%d, %d, %d); ",
	CurrentLineNumber,
	CurrentColumnNumber,
	CurrentColumnNumber-1+yyleng);
    strcat(Output, outputBuffer);
	}
	

[/][/][^\n]*[\n] {
    if(FilterEnabled==1)
    {
        CurrentColumnNumber=1;
        CurrentLineNumber=CurrentLineNumber+1;
    }
    else
    {
        char text[yyleng];
		int i;
        for( i=2; i<yyleng; i++)
		{text[i-2]=yytext[i]; }       
        text[yyleng-3]='\0';
        char outputBuffer[128];
        sprintf(outputBuffer,
		"comment(\"%s\", %d, %d, %d); ",
		text,
		CurrentLineNumber,
		CurrentColumnNumber,
		CurrentColumnNumber-1+yyleng);
        strcat(Output, outputBuffer);
        CurrentColumnNumber=1;
        CurrentLineNumber=CurrentLineNumber+1;
    }
}
[(][*]((([^*])*([^)])*)|((([^*])*([^)])*[*][^)]+[)]([^*])*([^)])*))*)[*][)] {
     int i;
	if(FilterEnabled==1)
    {       
        for(i=0; i<yyleng; i++)
        {
            CurrentColumnNumber=CurrentColumnNumber+1;
            if(yytext[i]=='\n')
            {
                CurrentColumnNumber=1;
                CurrentLineNumber=CurrentLineNumber+1;
            }
        }
    }
    else
    {
        
        int startCurrentLineNumber=CurrentLineNumber;
        int startCurrentColumnNumber=CurrentColumnNumber;
        int stopCurrentColumnNumber;
		
        for(i=0; i<yyleng; i++)
        {
            stopCurrentColumnNumber=CurrentColumnNumber;
            CurrentColumnNumber=CurrentColumnNumber+1;
            if(yytext[i]=='\n')
            {
                CurrentColumnNumber=1;
                CurrentLineNumber=CurrentLineNumber+1;
            }
        }
        char text[yyleng];
		 
        for(i=2; i<yyleng; i++)
		  text[i-2]=yytext[i];        
        text[yyleng-4]='\0';
        char outputBuffer[128];
        sprintf(outputBuffer,
		"comment(\"%s\", %d, %d, %d, %d); ",
		text, startCurrentLineNumber,
		CurrentLineNumber,
		startCurrentColumnNumber,
		stopCurrentColumnNumber);
        strcat(Output, outputBuffer);
    }
}
[*][*] {
    char outputBuffer[128];
    sprintf(outputBuffer,
	"operationOnVariablesPower(\"**\", %d, %d, %d); ",
	CurrentLineNumber, 
	CurrentColumnNumber, 
	CurrentColumnNumber-1+yyleng);
    strcat(Output, outputBuffer);
    CurrentColumnNumber=CurrentColumnNumber+yyleng;
}
	
[s][k][i][p] {
    char outputBuffer[128];
    sprintf(outputBuffer, 
	"skip(%d, %d, %d); ", 
	CurrentLineNumber,
	CurrentColumnNumber, 
	CurrentColumnNumber-1+yyleng);
    strcat(Output, outputBuffer);
    CurrentColumnNumber=CurrentColumnNumber+yyleng;
}
[:][=] {
    char outputBuffer[128];
    sprintf(outputBuffer, 
	"assigned(%d, %d, %d); ",
	CurrentLineNumber, 
	CurrentColumnNumber,
	CurrentColumnNumber-1+yyleng);
    strcat(Output, outputBuffer);
    CurrentColumnNumber=CurrentColumnNumber+yyleng;
}
[\;] {
    char outputBuffer[128];
    sprintf(outputBuffer,
	"newColumn(%d, %d, %d); ",
	CurrentLineNumber,
	CurrentColumnNumber, 
	CurrentColumnNumber-1+yyleng);
    strcat(Output, outputBuffer);
    CurrentColumnNumber=CurrentColumnNumber+yyleng;
}
[w][r][i][t][e] {
    char outputBuffer[128];
    sprintf(outputBuffer,
	"writeOperation(%d, %d, %d); ",
	CurrentLineNumber,
	CurrentColumnNumber,
	CurrentColumnNumber-1+yyleng);
    strcat(Output, outputBuffer);
    CurrentColumnNumber=CurrentColumnNumber+yyleng;
}
[r][e][a][d] {
    char outputBuffer[128];
    sprintf(outputBuffer,
	"readOperation(%d, %d, %d); ",
	CurrentLineNumber, 
	CurrentColumnNumber,
	CurrentColumnNumber-1+yyleng);
    strcat(Output, outputBuffer);
    CurrentColumnNumber=CurrentColumnNumber+yyleng;
}
[w][h][i][l][e] {
    char outputBuffer[128];
    sprintf(outputBuffer,
	"whileCicle(%d, %d, %d); ",
	CurrentLineNumber,
	CurrentColumnNumber,
	CurrentColumnNumber-1+yyleng);
    strcat(Output, outputBuffer);
    CurrentColumnNumber=CurrentColumnNumber+yyleng;
}
[d][o] {
    char outputBuffer[128];
    sprintf(outputBuffer,
	"doCicle(%d, %d, %d); ", 
	CurrentLineNumber,
	CurrentColumnNumber,
	CurrentColumnNumber-1+yyleng);
    strcat(Output, outputBuffer);
    CurrentColumnNumber=CurrentColumnNumber+yyleng;
}
[i][f] {
    char outputBuffer[128];
    sprintf(outputBuffer,
	"conditionIf(%d, %d, %d); ",
	CurrentLineNumber, 
	CurrentColumnNumber, 
	CurrentColumnNumber-1+yyleng);
    strcat(Output, outputBuffer);
    CurrentColumnNumber=CurrentColumnNumber+yyleng;
}
[t][h][e][n] {
    char outputBuffer[128];
    sprintf(outputBuffer,
	"conditionThen(%d, %d, %d); ",
	CurrentLineNumber, CurrentColumnNumber,
	CurrentColumnNumber-1+yyleng);
    strcat(Output, outputBuffer);
    CurrentColumnNumber=CurrentColumnNumber+yyleng;
}
[e][l][s][e] {
    char outputBuffer[128];
    sprintf(outputBuffer,
	"conditionElse(%d, %d, %d); ",
	CurrentLineNumber, 
	CurrentColumnNumber, 
	CurrentColumnNumber-1+yyleng);
    strcat(Output, outputBuffer);
    CurrentColumnNumber=CurrentColumnNumber+yyleng;
}
[a-zA-Z_][a-zA-Z_0-9]* {yytext [yyleng] = (char) 0;
    char outputBuffer[128];
    sprintf(outputBuffer,
	"variable(\"%s\", %d, %d, %d); ", 
	yytext, CurrentLineNumber,
	CurrentColumnNumber,
	CurrentColumnNumber-1+yyleng);
    strcat(Output, outputBuffer);
    CurrentColumnNumber=CurrentColumnNumber+yyleng;
}
([+|\-|/|%|>|<])|([=|\!][=])|([>|<][=])|([&][&])|([\|][\|]) {
    yytext [yyleng] = (char) 0;
    char outputBuffer[128];
    sprintf(outputBuffer,
	"operationOnVariables(\"%s\", %d, %d, %d); ",
	yytext, CurrentLineNumber,
	CurrentColumnNumber,
	CurrentColumnNumber-1+yyleng);
    strcat(Output, outputBuffer);
    CurrentColumnNumber=CurrentColumnNumber+yyleng;
}
([*]) {
    char outputBuffer[128];
    sprintf(outputBuffer,
	"operationOnVariables(\"%s\", %d, %d, %d); ",
	yytext, CurrentLineNumber,
	CurrentColumnNumber,
	CurrentColumnNumber-1+yyleng);
    strcat(Output, outputBuffer);
    CurrentColumnNumber=CurrentColumnNumber+yyleng;
}

[1-9][0-9]*[a-zA-Z] {
	char outputBuffer[128];
    sprintf(outputBuffer, 
	"Error: line %d, columns %d - %d\n",
	CurrentLineNumber, 
	CurrentColumnNumber, 
	CurrentColumnNumber-1+yyleng 
	);
    strcat(Output, outputBuffer);
    return;
}

	
[1-9][0-9]* {
yytext [yyleng] = (char) 0;
char outputBuffer[128];
sprintf(outputBuffer, 
"number(\"%s\", %d, %d, %d); ", 
yytext,
 CurrentLineNumber,
 CurrentColumnNumber, 
 CurrentColumnNumber-1+yyleng);
strcat(Output, outputBuffer);
CurrentColumnNumber=CurrentColumnNumber+yyleng;
  }
  
([\(])|([\)]) {
    yytext [yyleng] = (char) 0;
    char outputBuffer[128];
    sprintf(outputBuffer, 
	"bracket(\"%s\", %d, %d, %d); ",
	yytext,
	CurrentLineNumber, 
	CurrentColumnNumber, 
	CurrentColumnNumber-1+yyleng);
    strcat(Output, outputBuffer);
    CurrentColumnNumber=CurrentColumnNumber+yyleng;
}
[^ \r\n] {
    char outputBuffer[128];
    sprintf(outputBuffer, 
	"Error: line %d, columns %d - %d: \"%s\"\n",
	CurrentLineNumber, 
	CurrentColumnNumber, 
	CurrentColumnNumber-1+yyleng, 
	yytext);
    strcat(Output, outputBuffer);
    return;
}
 
%%

main(int argc,char *argv[])
{

    if(strcmp(argv[1], "-filter"))
    {
		yyin=fopen(argv[1],"r");        
    }
    else{
		FilterEnabled=1;
        yyin=fopen(argv[2],"r");
	}

    if(yyin==NULL)
        printf("ERROR: File not Exists");
    else
    {
        yylex();
        printf("%s\n\n", Output);
    }
	

}

