Собиралось на bash on windows, но для терминала ubuntu тоже годится

Требования:
sudo apt-get install gcc
sudo apt-get install byacc flex

В репозитории уже есть бинарник lxr, поэтому собирать не обязательно, но если очень надо, то 

lex source.l
gcc lex.yy.c -lfl -o lxr

или вызвать так ./compile


Чтобы запустить 

./lxr filename

или 

./lxr -filter filename 

Пример ./lxr TestFromHW09.txt

Если вводить атрибут  -filter, то не будут выводиться комментарии

Выводятся строки вида:
function(номер строки, номер столбца начала, номер столбца конца);
